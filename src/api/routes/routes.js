const shorten = require("../controllers/shorteningController");

const routes = (app) => {
  app.route("/shorten").post(shorten.saveShorten);
  app.route("/").get(shorten.getshorten);
  app.route("/domain").get(shorten.listDomain);
};

module.exports = routes;
