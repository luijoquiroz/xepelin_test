const shortModel = require("../../databases/models/shortModel");

module.exports = {
  async saveShorten(req, res) {
    if (typeof req.body.url !== "string") {
      res.status(400).json({
        success: false,
        messager: "Parametros Incorrectos",
      });
      return res.end();
    }

    const url = new URL(req.body.url);

    console.log(`[controller] saveShorten: url${url}`);
    console.log(url);

    const result = await shortModel.findOne({ domain: url.hostname }).exec();

    console.log(`[controller] saveShorten: findOne${result}`);

    if (!result) {
      const document = new shortModel({
        domain: url.hostname,
      });
      document.save((err, document) => {
        console.log(`[controller] saveShorten: save ${document}`);
        if (err) {
          res.status(500).json({
            success: false,
            message: "Error interno servidor",
          });
          return res.end();
        }
        res.status(201).json({
          success: true,
          id: document.domainId,
        });
        return res.end();
      });
    }
    // console.log(result);
    res.status(208).json({
      success: false,
      id: result.domainId,
    });
    return res.end();
  },
  async getshorten(req, res) {
    if (typeof req.query !== "object") {
      res.status(400).json({
        success: false,
        messager: "Parametros Incorrectos",
      });
      return res.end();
    }

    const { query } = req;

    // console.log(`[controller] getshorten: query ${query}`);
    const result = await shortModel.findOne({ domainId: query.id }).exec();
    // console.log(`[controller] getshorten: findOne${result}`);

    if (!result) {
      res.status(404).json({
        success: false,
      });
      return res.end();
    }
    // console.log(result);
    res.status(208).json({
      success: true,
      url: result.domain,
    });
    return res.end();
  },
  async listDomain(req, res) {
    // const url = new URL(req.body.url);
    const result = await shortModel.find({}).exec();
    // console.log(`[controller] listDomain: result${result}`);
    if (!result) {
      res.status(500).json({
        success: false,
        message: "Error interno servidor",
      });
      return res.end();
    }
    // console.log(result);
    const data = result.map((item) => {
      const subdomain = "www.";
      return item.domain.slice(subdomain.length);
    });

    res.json({
      success: false,
      data: data,
    });
    return res.end();
  },
};
