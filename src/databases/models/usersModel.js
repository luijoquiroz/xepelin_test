const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  nombre: String,
  apellido: String,
  email: String,
  password: String,
  createdAt: { type: Date, default: Date.now },
  createdBy: String,
  updatedAt: { type: Date },
  updatedBy: String,
});
module.exports = mongoose.model("user", userSchema);
