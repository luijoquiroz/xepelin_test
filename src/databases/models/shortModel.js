const mongoose = require("mongoose");
const shortId = require("shortid");

const shortUrlSchema = new mongoose.Schema({
  domainId: {
    type: String,
    required: true,
    default: shortId.generate,
  },
  domain: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Shortener", shortUrlSchema);
