const mongoose = require("mongoose");
const config = require("../config/config");

const connection = () => {
  // console.log(`MONGODB_URI ${config.MONGO_URI} `);

  mongoose.connect(config.MONGO_URI, {
    server: { socketOptions: { keepAlive: 1 } },
  });

  mongoose.connection.on("connected", () => {
    console.log(`mongoose[connected]`);
  });

  mongoose.connection.on("error", (err) => {
    console.log(`mongoose[error] ${err}`);
  });

  mongoose.connection.on("disconnected", () => {
    console.log(`mongoose[disconnected] `);
  });
};

module.exports = connection;
