const express = require("express");
const bootstrap = require("./bootstrap/bootstrapApp");
const config = require("./config/config");
const app = express();

bootstrap(app);

app.set("port", config.PORT);

const server = app.listen(app.get("port"), () => {
  console.log(
    `Server ${config.APPLICATION_NAME} runnig in port ${server.address().port}`
  );
});
